var Bitly = require('bitly');
var async = require('async');
var Utils = require('./utils.js');
var colors = require('colors/safe');
var LastfmAPI = require('lastfmapi');
var configBitly = require('../config/configBitly');
var configLastFM = require('../config/configLastFM');

//API Keys
var bitly = new Bitly(configBitly.bitly_username, configBitly.bitly_api_key);
var lfm = new LastfmAPI({'api_key' : configLastFM.lastfm_api_key, 'secret' : configLastFM.lastfm_secret});

module.exports = {

    // Retrieve data from a specific SONG from a specific ARTIST
    getInfoFromTrack: function(screenName,track,artist,tweetCallback,tweetID) {
        lfm.track.getInfo({
            'artist' : artist,
            'track' : track,
            'autocorrect' : 1
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT " + track.toUpperCase() + " BY " + artist.toUpperCase());
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            var URL = data['url'];
            var listeners = data['listeners'];
            var playcount = data['playcount'];
            if(data['album'] != undefined) {
                var album = data['album']['title'];
            }

            bitly.shorten(URL, function(err, response) {
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var tweetAnswer;
                var short_url = response.data.url;
                if(album) {
                    tweetAnswer = Utils.generateTweetResponse("Hi %s, this song is from the album %s. It got played %s times by %s listeners - %s",
                        {value:screenName,  color:"green"},
                        {value:album,       color:"cyan"},
                        {value:playcount,   color:"cyan"},
                        {value:listeners,   color:"cyan"},
                        {value:short_url,   color:"cyan"}
                    );
                    tweetCallback(tweetAnswer,screenName,tweetID);
                } else {
                    tweetAnswer = Utils.generateTweetResponse("Hi %s, this song is not part of any album. It got played %s times by %s listeners - %s",
                        {value:screenName,  color:"green"},
                        {value:playcount,   color:"cyan"},
                        {value:listeners,   color:"cyan"},
                        {value:short_url,   color:"cyan"}
                    );
                    tweetCallback(tweetAnswer,screenName,tweetID);
                }
            });
        });
    },

    // Retrieve data from a specific SONG from a specific ARTIST
    getDetailsFromArtist: function(screenName,artist,tweetCallback,tweetID) {
        lfm.artist.getInfo({
            'artist' : artist,
            'autocorrect' : 1
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT " + artist.toUpperCase());
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            var URL = data['url'];
            var listeners = data['stats']['listeners'];
            var playcount = data['stats']['playcount'];
            var placeformed = data['bio']['placeformed'];

            bitly.shorten(URL, function(err, response) {
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var short_url = response.data.url;
                var tweetAnswer = Utils.generateTweetResponse("Hi %s, %s is from %s (has been played %s times by %s listeners) - %s",
                    {value:screenName,  color:"green"},
                    {value:artist,       color:"green"},
                    {value:placeformed,   color:"cyan"},
                    {value:playcount,   color:"cyan"},
                    {value:listeners,   color:"cyan"},
                    {value:short_url,   color:"cyan"}
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    },

    // Retrieve data from all SONGS similar to a specific SONG
    findSimilarTracks: function(screenName,track,artist,tweetCallback,tweetID) {
        lfm.track.getSimilar({
            'artist' : artist,
            'track' : track,
            'autocorrect' : 1,
            'limit': 3
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT TRACKS SIMILAR TO " + track.toUpperCase() + " BY " + artist.toUpperCase());
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            async.series([
                function(callback){
                    bitly.shorten(data['track'][0]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][1]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][2]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                }
            ], function(err, results){
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var track01 = data['track'][0]['name'] + " by " + data['track'][0]['artist']['name'] + " (" + results[0] + ")";
                var track02 = data['track'][1]['name'] + " by " + data['track'][1]['artist']['name'] + " (" + results[1] + ")";
                var track03 = data['track'][2]['name'] + " by " + data['track'][2]['artist']['name'] + " (" + results[2] + ")";
                var tweetAnswer = Utils.generateTweetResponseFromArray("Hi %s, try these: ",
                    {value: screenName, color:"green"},
                    [

                        {value:track01, color:"cyan"},
                        {value:track02, color:"cyan"},
                        {value:track03, color:"cyan"}
                    ]
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    },

    // Retrieve data from all ARTISTS similar to a specific ARTIST
    findSimilarArtist: function(screenName,artist,tweetCallback,tweetID) {
        lfm.artist.getSimilar({
            'artist' : artist,
            'autocorrect' : 1,
            'limit': 3
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT ARTISTS SIMILAR TO " + artist.toUpperCase());
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            async.series([
                function(callback){
                    bitly.shorten(fixURI(data['artist'][0]['url']), function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(fixURI(data['artist'][1]['url']), function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(fixURI(data['artist'][2]['url']), function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                }
            ], function(err, results){
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var artist01 = data['artist'][0]['name'] + " (" + results[0] + ")";
                var artist02 = data['artist'][1]['name'] + " (" + results[1] + ")";
                var artist03 = data['artist'][2]['name'] + " (" + results[2] + ")";
                var tweetAnswer = Utils.generateTweetResponseFromArray("Hi %s, try these: ",
                    {value: screenName, color:"green"},
                    [

                        {value:artist01, color:"cyan"},
                        {value:artist02, color:"cyan"},
                        {value:artist03, color:"cyan"}
                    ]
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    },

    // Retrieve data from the most popular SONGS from a specific ARTIST
    findTopTracksFromArtist: function(screenName,artist,tweetCallback,tweetID) {
        lfm.artist.getTopTracks({
            'artist' : artist,
            'autocorrect' : 1,
            'limit': 3
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT TOP TRACKS FROM " + artist.toUpperCase());
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            async.series([
                function(callback){
                    bitly.shorten(data['track'][0]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][1]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][2]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                }
            ], function(err, results){
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var track01 = data['track'][0]['name'] + " (" + results[0] + ")";
                var track02 = data['track'][1]['name'] + " (" + results[1] + ")";
                var track03 = data['track'][2]['name'] + " (" + results[2] + ")";
                var tweetAnswer = Utils.generateTweetResponseFromArray("Here you go %s: ",
                    {value: screenName, color:"green"},
                    [

                        {value:track01, color:"cyan"},
                        {value:track02, color:"cyan"},
                        {value:track03, color:"cyan"}
                    ]
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    },

    // Retrieve data from most popular ARTISTS at the given time
    getTopArtists: function(screenName,tweetCallback,tweetID) {
        lfm.chart.getTopArtists({
            'limit': 3
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT TOP ARTISTS");
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            async.series([
                function(callback){
                    bitly.shorten(data['artist'][0]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['artist'][1]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['artist'][2]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                }
            ], function(err, results){
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var artist01 = data['artist'][0]['name'] + " (" + results[0] + ")";
                var artist02 = data['artist'][1]['name'] + " (" + results[1] + ")";
                var artist03 = data['artist'][2]['name'] + " (" + results[2] + ")";
                var tweetAnswer = Utils.generateTweetResponseFromArray("Here you go %s: ",
                    {value: screenName, color:"green"},
                    [

                        {value:artist01, color:"cyan"},
                        {value:artist02, color:"cyan"},
                        {value:artist03, color:"cyan"}
                    ]
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    },

    // Retrieve data from most loved SONGS at the given time
    getLovedTracks: function(screenName,tweetCallback,tweetID) {
        lfm.chart.getLovedTracks({
            'limit': 3
        }, function (err, data) {
            Utils.displaySubtitle("QUERYING LASTFM ABOUT LOVED TRACKS");
            if (err) return handleError(screenName,err,tweetCallback,tweetID);
            async.series([
                function(callback){
                    bitly.shorten(data['track'][0]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][1]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                },
                function(callback){
                    bitly.shorten(data['track'][2]['url'], function(err, response) {
                        if (err) return handleError(screenName,err,tweetCallback,tweetID);
                        callback(null, response.data.url);
                    });
                }
            ], function(err, results){
                if (err) return handleError(screenName,err,tweetCallback,tweetID);
                var track01 = data['track'][0]['name'] + " by " + data['track'][0]['artist']['name'] + " (" + results[0] + ")";
                var track02 = data['track'][1]['name'] + " by " + data['track'][1]['artist']['name'] + " (" + results[1] + ")";
                var track03 = data['track'][2]['name'] + " by " + data['track'][2]['artist']['name'] + " (" + results[2] + ")";
                var tweetAnswer = Utils.generateTweetResponseFromArray("Here you go %s: ",
                    {value: screenName, color:"green"},
                    [

                        {value:track01, color:"cyan"},
                        {value:track02, color:"cyan"},
                        {value:track03, color:"cyan"}
                    ]
                );
                tweetCallback(tweetAnswer,screenName,tweetID);
            });
        });
    }
};

// Bitly may not function if the URI parameter does not start with http://"
// This function is used to only append it, if necessary
function fixURI(uri) {
    var expectedHTTP = "http://";
    var expectedHTTPS = "https://";
    if((uri.lastIndexOf(expectedHTTP, 0) !== 0) || (uri.lastIndexOf(expectedHTTPS, 0) !== 0)) {
        return expectedHTTP + uri;
    }
    return uri;
}

// Function used to generate Tweet answer if no data could be found
function handleError(screenName,err,tweetCallback,tweetID) {
    Utils.displayString("%s: %s",
        {value:'Response status:', color:"red"},
        {value:err['error']}
    );
    Utils.displayString("%s: %s",
        {value:'Message:', color:"red"},
        {value:err['message']}
    );
    Utils.displayString("%s: %s",
        {value:'Details', color:"red"},
        {value:JSON.stringify(err,null,2)}
    );
    var tweetAnswer = Utils.generateTweetResponse("Sorry %s, I can't find anything about this song/artist. Maybe try another one?",
        {value:screenName, color:"green"}
    );
    tweetCallback(tweetAnswer,screenName,tweetID);
}